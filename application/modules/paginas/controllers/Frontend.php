<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array(),TRUE),
                    'link'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                                
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('telefono','Telefono','required');   
            $this->form_validation->set_rules('politicas','Politicas','required');            
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $_SESSION['msj'] = $this->error('Captcha introduit incorrectament');
                }else{
                    $this->enviarcorreo((object)$_POST,1,'info@jordimagana.com');
                    unset($_POST['politicas']);
                    unset($_POST['form-check']);
                    unset($_POST['subject']);
                    unset($_POST['fields']);
                    $this->db->insert('contacto',$_POST);
                    $_SESSION['msj'] = 'Graciés por contactar\'ns, en breve et trucaremos';                    
                }
            }else{                
               $_SESSION['msj'] = 'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>';
            }
            //header('Location:'.base_url().'contacta.html#contacto');
            //echo json_encode(array('text'=>$_SESSION['msj']));
            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }

        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('politicas','Políticas','required');
            if($this->form_validation->run()){
                
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = 'Subscripción exitosa';
                    $err = 'success';
                    $this->enviarcorreo((object)$_POST,10);
                }else{
                    $_SESSION['msj2'] = 'Ya el correo esta registrado';
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo json_encode(array('result'=>$err,'msg'=>$_SESSION['msj2']));
            unset($_SESSION['msj2']);
            //redirect(base_url().'p/contacte#subscribir');
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }

        function sitemap(){
            $pages = array(
                '',
                'contacte.html',
                'nosaltres.html',
                'serveis.html',
                'portafolio.html',
                'blog',                
            );

            //Blogs
            foreach($this->db->get_where('blog',array('blog_categorias_id'=>4))->result() as $b){
                $pages[] = 'blog/'.toURL($b->id.'-'.$b->titulo);
            }

            //Serveis
            foreach($this->db->get_where('blog',array('blog_categorias_id'=>2))->result() as $b){
                $pages[] = 'serveis.html#serveis'.$b->id;
            }

            //Portafolio
            foreach($this->db->get_where('blog',array('blog_categorias_id'=>3))->result() as $b){
                if(strpos($b->texto,'http://') || strpos($b->texto,'https://')){
                    $url = strip_tags($b->texto);
                }
                elseif(strpos($b->texto,'www')){
                    $url = "http://".strip_tags($b->texto);
                }
                else{
                    $url = 'img/blog/'.$b->foto;
                }

                $pages[] = $url;
            }
            
            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $p = base_url().$p;
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }

        function search(){
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3Amovilessat.es+'.urlencode($_GET['q']).'&oq=site%3Adiscotecarecords.com+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }
                $result = $_SESSION[$_GET['q']];
                preg_match_all('@<div id=\"ires\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = fragmentar($resultado,'<ol>','</ol>');
                $resultado = $resultado[0];

           
                $resultado = str_replace('https://www.movilessat.es/url?q=','',$resultado);
                $resultado = str_replace('/url?q=','',$resultado);
                $resultado = explode('<div class="g">',$resultado);                
                foreach($resultado as $n=>$r){
                    if(strpos($r,'/search?q=site:')){
                        unset($resultado[$n]);
                        continue;
                    }
                    $resultado[$n] = '<div class="g">'.$r;                    
                    $resultado[$n] = substr($r,0,strpos($r,'&'));
                    $pos = strpos($r,'">')+2;
                    $rr = substr($r,$pos);
                    $pos = strpos($rr,'">');
                    $rr = substr($rr,$pos);                    
                    $resultado[$n].= $rr;
                    $resultado[$n] = '<div class="g">'.$resultado[$n];
                    $resultado[$n] = utf8_encode($resultado[$n]);
                }
                
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }


    }
