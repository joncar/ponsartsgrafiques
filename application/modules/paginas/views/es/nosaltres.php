<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
	<div id="box_wrapper">
		<?php $this->load->view('es/header',array('act'=>2),FALSE,'paginas'); ?>
		<section class="page_breadcrumbs ds background_cover section_padding_25">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h1>Qui som?</h1>
							<ol class="breadcrumb darklinks">
								<li> <a href="./">
							Home
						</a> </li>
								<!-- <li> <a href="#">Pages</a> </li> -->
								<li class="active"> <span>UNA MICA DE NOSALTRES</span> </li>
							</ol>
						</div>
					</div>
				</div>
			</section>
			<section id="about-single" class="ls section_padding_top_150 section_padding_bottom_150">
				<div class="container">
					<div class="row">
						<div class="col-xs-12"> <img src="<?= base_url() ?>theme/images/about.png" alt="" class="alignleft cover-image"> <span class="above_heading highlight">Benvinguts</span>
							<h2 class="section_header">Pons Arts Gràfiques is the leader in defferent format printing</h2>
							<p>With over 30 years of experience, we specialize in variety of high quality services and products including wall murals, event graphics, tradeshow displays, vehicle wraps, and window graphics. Mea congue diceret amet singulis id, accusam legendos
								accusata eum ad, quo facer malorum maiestatis ea. Quo cu pertinax assentior vulputate, id ullum boborum impetus amet ludus has ex In eam nulla veniam accusam. Elit evertitur veritus sale duo. Has at purto patrioque, eu mei augue splendide eu ius
								soluta ignota nonumes. No duo autem tacimates. His no appetere has singulis, ex mutat dicam pri. Iisque appetere sed id, in has persecuti rationibus cum. Movet abhorreant persequeris mea et, at nihil vituperata mei. Id saepe intellegebat quo,
								vel ei dicunt ludus.</p>
						</div>
					</div>
				</div>
			</section>
			<section id="featured-video" class="cs main_color5 overlay_color parallax section_padding_top_150 section_padding_bottom_150">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center"> <a href="https://www.youtube.com/watch?v=2Kvl0vpV6lM" class="theme_button bg_button color3 round_button play_button margin_0" data-gal="prettyPhoto[gal-video]">
					<i class="fa fa-play" aria-hidden="true"></i>
				</a>
							<div class="toppadding_30 visible-lg"></div>
							<div class="toppadding_30"></div> <span class="above_heading highlight2">Video Presentació</span>
							<h2 class="section_header black">Les nostes instal•lacions</h2>
						</div>
					</div>
				</div>
			</section>
			<section id="products" class="ls section_padding_top_150 section_padding_bottom_145">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center"> <span class="above_heading highlight">Productes</span>
							<h2 class="section_header">Us ajudem a fer<br> el vostre projecte </h2>
							<div class="row flex-wrap loop-colors columns_padding_30 columns_margin_bottom_50">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="teaser hover-teaser main_bg_color big-padding text-center">
										<div>
											<div class="teaser_icon highlight size_small"> <i class="clr-service-card"></i> </div>
											<h3 class="entry-title small bottommargin_0">Targetes de visita</h3>
											<p>Des de 9.99€</p>
										</div> <a href="shop.html" class="abs-link"></a> </div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="teaser hover-teaser main_bg_color big-padding text-center">
										<div>
											<div class="teaser_icon highlight size_small"> <i class="clr-service-booklet"></i> </div>
											<h3 class="entry-title small bottommargin_0">Catàlegs</h3>
											<p>Des de 9.99€</p>
										</div> <a href="shop.html" class="abs-link"></a> </div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="teaser hover-teaser main_bg_color big-padding text-center">
										<div>
											<div class="teaser_icon highlight size_small"> <i class="clr-service-copy"></i> </div>
											<h3 class="entry-title small bottommargin_0">Impressió Digital</h3>
											<p>Des de 9.99€</p>
										</div> <a href="shop.html" class="abs-link"></a> </div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="teaser hover-teaser main_bg_color big-padding text-center">
										<div>
											<div class="teaser_icon highlight size_small"> <i class="clr-service-t-shirt"></i> </div>
											<h3 class="entry-title small bottommargin_0">Merchandaising</h3>
											<p>Des de 9.99€</p>
										</div> <a href="shop.html" class="abs-link"></a> </div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="teaser hover-teaser main_bg_color big-padding text-center">
										<div>
											<div class="teaser_icon highlight size_small"> <i class="clr-service-calendar"></i> </div>
											<h3 class="entry-title small bottommargin_0">Calendaris</h3>
											<p>Des de 9.99€</p>
										</div> <a href="shop.html" class="abs-link"></a> </div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="teaser hover-teaser main_bg_color big-padding text-center">
										<div>
											<div class="teaser_icon highlight size_small"> <i class="clr-service-banner"></i> </div>
											<h3 class="entry-title small bottommargin_0">Packaging</h3>
											<p>Des de 9.99€</p>
										</div> <a href="shop.html" class="abs-link"></a> </div>
								</div>
							</div>
							<div class="toppadding_20"></div>
							<div class="toppadding_20 visible-lg"></div> <a href="events-full.html" class="theme_button color1">Veure serveis</a> </div>
					</div>
				</div>
			</section>

		<section class="ls section_padding_top_150 section_padding_bottom_150">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="isotope_container isotope row masonry-layout columns_margin_bottom_20 loop-colors">
								<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-1.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Julia Moore</a></h3>
											<p class="small-text highlight bottommargin_20">graphic designer</p>
										</div>										
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-2.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Laura Conors</a></h3>
											<p class="small-text highlight bottommargin_20">Director</p>
										</div>										
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-3.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Evan Alsopp</a></h3>
											<p class="small-text highlight bottommargin_20">manager</p>
										</div>										
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-4.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Adrian Fraser</a></h3>
											<p class="small-text highlight bottommargin_20">Print Supervisor</p>
										</div>										
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-5.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Verity Benson</a></h3>
											<p class="small-text highlight bottommargin_20">tech-Designer</p>
										</div>										
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-6.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Cheryl Lopez</a></h3>
											<p class="small-text highlight bottommargin_20">General manager</p>
										</div>										
									</article>
								</div>
								<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-6.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Cheryl Lopez</a></h3>
											<p class="small-text highlight bottommargin_20">General manager</p>
										</div>										
									</article>
								</div>

<div class="isotope-item col-xs-12 col-sm-6 col-lg-3">
									<article class="team-item vertical-item content-padding big-padding with_border text-center">
										<div class="item-media-wrap">
											<div class="item-media"> <img src="<?= base_url() ?>theme/images/team/team-6.jpg" alt=""> <a href="team-single.html" class="abs-link"></a> </div>
										</div>
										<div class="item-content">
											<h3 class="entry-title"><a href="team-single.html">Cheryl Lopez</a></h3>
											<p class="small-text highlight bottommargin_20">General manager</p>
										</div>										
									</article>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>
		<?php $this->load->view('footer',array('es/footer'),FALSE,'paginas'); ?>
	</div>
	<!-- eof #box_wrapper -->
</div>
	<!-- eof #canvas -->
<?php $this->load->view('es/scripts',array(),FALSE,'paginas'); ?>