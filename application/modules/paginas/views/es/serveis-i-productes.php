<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
	<div id="box_wrapper">
		<?php $this->load->view('es/header',array('act'=>3),FALSE,'paginas'); ?>
		<section class="page_breadcrumbs ds background_cover section_padding_25">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h1>SERVEIS I PRODUCTES</h1>
							<ol class="breadcrumb darklinks">
								<li> <a href="./">
							Home
						</a> </li>
								<!-- <li> <a href="#">Events</a> </li> -->
								<li class="active"> <span>TOTO LO QUE SABEM FER</span> </li>
							</ol>
						</div>
					</div>
				</div>
			</section>
			<section class="ls section_padding_top_150 section_padding_bottom_150 columns_padding_30">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/01.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>									
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> 
														<time datetime="2017-10-03T08:50:40+00:00">20 june, 2018 8:00 am</time>
													</div>
												</div>
												<h3 class="entry-title"> 
													<a href="javascript:void(0)">Hinc Zril Admodum Mea Qui Has Impetus</a>
												</h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/02.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> <time datetime="2017-10-03T08:50:40+00:00">21 june, 2018  9:00 am</time> </div>
												</div>
												<h3 class="entry-title"> <a href="javascript:void(0)">Sed diam et dolore magna</a> </h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/03.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> <time datetime="2017-10-03T08:50:40+00:00">22 june, 2018  10:00 am</time> </div>
												</div>
												<h3 class="entry-title"> <a href="javascript:void(0)">Consetetur sadipscing elitr</a> </h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/04.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> <time datetime="2017-10-03T08:50:40+00:00">23 june, 2018  11:00 am</time> </div>
												</div>
												<h3 class="entry-title"> <a href="javascript:void(0)">Diam et dolore magna aliquyam</a> </h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/05.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> <time datetime="2017-10-03T08:50:40+00:00">24 june, 2018  12:00 am</time> </div>
												</div>
												<h3 class="entry-title"> <a href="javascript:void(0)">Sadipscing elitr sed diam</a> </h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/06.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> <time datetime="2017-10-03T08:50:40+00:00">25 june, 2018  1:00 pm</time> </div>
												</div>
												<h3 class="entry-title"> <a href="javascript:void(0)">Ipsum dolor sit amet consetetur</a> </h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/07.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> <time datetime="2017-10-03T08:50:40+00:00">26 june, 2018  2:00 pm</time> </div>
												</div>
												<h3 class="entry-title"> <a href="javascript:void(0)">Magna aliquyam erased voluptua</a> </h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>
							<article class="post event-item side-item side-sm content-padding with_border">
								<div class="logoServei"><i class="clr-service-printing" style="font-size:60px"></i></div>
								<div class="row">
									<div class="col-sm-5">
										<div class="item-media"> <img src="<?= base_url() ?>theme/images/events/08.jpg" alt=""> <a class="abs-link" title="" href="javascript:void(0)"></a> </div>
									</div>
									<div class="col-sm-7">
										<div class="item-content">
											<header class="entry-header">
												<div class="entry-meta small-text">
													<div class="highlight"> <time datetime="2017-10-03T08:50:40+00:00">27 june, 2018  3:00 pm</time> </div>
												</div>
												<h3 class="entry-title"> <a href="javascript:void(0)">Iudico Nusquam Principes Argumentum</a> </h3>
											</header>
											<div class="content-3lines-ellipsis">
												<p>
													Proin sed placerat justo, iaculis mattis ligula. Suspendisse vitae risus urna. Suspendisse nec laoreet justo. Etiam in nunc ullamcorper, dapibus dui ac, ullamcorper nisl.  <br/><br/>
													<a href="#pedirPresupuesto" data-toggle="modal" class="theme_button color1">Demana pressupost</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</article>							
						</div>
						<!--eof .col-sm-* (main content)-->
					</div>
				</div>
			</section>
		<?php $this->load->view('footer',array('es/footer'),FALSE,'paginas'); ?>
	</div>
	<!-- eof #box_wrapper -->
</div>


<!-- search modal -->
<div class="modal" tabindex="-1" role="dialog" id="pedirPresupuesto">
	<div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Demanar pressupost</h4>
	      </div>
	      <div class="modal-body" style="padding:0">
	        <div class="cs contact-form-wrapper background_cover with_padding big-padding">
				<form class="contact-form" method="post" action="./">
					<div class="row columns_margin_bottom_20 bottommargin_0">
						<div class="col-xs-12 col-sm-6">
							<div class="form-group"> <label for="name">Nom i cognoms</label> <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Nom i cognoms"> </div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group"> <label for="email">Email</label> <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email"> </div>
						</div>
						<div class="col-xs-12">
							<div class="form-group"> <label for="phone">Telèfon</label> <input type="tel" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Telèfon"> </div>
						</div>
						<div class="col-xs-12">
							<div class="form-group"> <label for="message">el teu missatge</label> <textarea aria-required="true" rows="3" cols="45" name="message" id="message" class="form-control" placeholder="el teu missatge"></textarea> </div>
						</div>
						<div class="col-xs-12 bottommargin_0">
							<div class="contact-form-submit"> <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1">Enviar Missatge</button> </div>
						</div>
					</div>
				</form>
			</div>
	      </div>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div>



	<!-- eof #canvas -->
<?php $this->load->view('es/scripts',array(),FALSE,'paginas'); ?>