<footer class="page_footer cs main_color7 section_padding_top_110 section_padding_bottom_120 columns_margin_bottom_30">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<div class="teaser text-center">
					<div class="teaser_icon size_big highlight3"> <img src="<?= base_url() ?>theme/images/clr-phone-call-3.png" alt=""> </div>
					<p> <span class="small-text highlight3">Truca'ns ara</span><br> <span class="big black">93 804 47 91</span> </p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<div class="teaser text-center">
					<div class="teaser_icon size_big highlight8"> <img src="<?= base_url() ?>theme/images/clr-mail-5.png" alt=""> </div>
					<p> <span class="small-text highlight8">CONTACTA'NS</span><br> <span class="big black darklinks"><a href="mailto:#">info@ponsartsgrafiques.com</a></span> </p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<div class="teaser text-center">
					<div class="teaser_icon size_big highlight9"> <img src="<?= base_url() ?>theme/images/clr-clock-5.png" alt=""> </div>
					<p> <span class="small-text highlight9">5 dies a la setmana</span><br> <span class="big black">9:00am - 8:00PM</span> </p>
				</div>
			</div>
		</div>
	</div>
</footer>
<section class="page_copyright cs section_padding_top_110 section_padding_bottom_100">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<p class="page_social bottommargin_20"> <a href="#" class="social-icon light-bg-icon color-icon rounded-icon socicon-facebook"></a> <a href="#" class="social-icon light-bg-icon color-icon rounded-icon socicon-twitter"></a></a>								</p>
				<p class="small-text black">&copy; Copyright 2019 Drets reservats / Avis legal</p>
			</div>
		</div>
	</div>
</section>