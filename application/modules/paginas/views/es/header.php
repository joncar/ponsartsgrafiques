<!-- template sections -->
<div class="transparent_wrapper">
	<header class="page_header header_v2 header_white toggler_xxs_right">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 display-flex v-center">
					<div class="header_left_logo"> 
						<a href="./" class="logo">
                            <img src="<?= base_url() ?>theme/images/logo-light.png" alt="">
                            <img src="<?= base_url() ?>theme/images/logo.png" alt="">
                        </a> 
        			</div>
					<div class="header_mainmenu text-center">									
						<?= $this->load->view('es/menu',array(),FALSE,'paginas') ?>									
						<span class="toggle_menu"><span></span></span>
					</div>
					<div class="header_right_buttons text-right hidden-xxs">
						<p class="page_social bottommargin_25"> <a href="#" class="social-icon color-bg-icon rounded-icon socicon-facebook"></a> <a href="#" class="social-icon color-bg-icon rounded-icon socicon-twitter"></a> <a href="#" class="social-icon color-bg-icon rounded-icon socicon-googleplus"></a>										</p>
					</div>
				</div>
			</div>
		</div>
	</header>
</div>