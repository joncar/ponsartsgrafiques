	<div id="canvas">
		<div id="box_wrapper">
			<?php $this->load->view('es/headerMain',array(),FALSE,'paginas'); ?>
			<section class="intro_section page_mainslider cs all-scr-cover image-dependant">
				<div class="flexslider" data-dots="false" data-nav="true">
					<ul class="slides">
						<li>
							<div class="slide-image-wrap"> <img src="<?= base_url() ?>theme/images/slide01.jpg" alt="" /> </div>
							<div class="container">
								<div class="row">
									<div class="col-xs-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer to_animate" data-animation="fadeInUp">
													<h1 class="black">Més que imprimir... <br>...impressionem!!</h1>
												</div>
												<div class="intro-layer to_animate" data-animation="fadeInUp">
													<div class="slide_buttons"> <a href="file-upload.html" class="theme_button bg_button color2 min_width_button"><i class="fa fa-upload rightpadding_5"></i> Pujar arxius</a> <a href="#" class="theme_button bg_button color1 min_width_button"><i class="fa fa-pencil leftpadding_5"></i> Demana Pressupost</a>														</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section>
			<section id="subscribe" class="cs main_color2 half_cs section_padding_top_40 section_padding_bottom_40 columns_padding_80">
				<div class="container">
					<div class="row flex-wrap v-center">
						<div class="col-xs-12 col-sm-6 text-center text-sm-right">
							<div class="toppadding_25 hidden-xs"></div>
							<div class="small-text greylinks topmargin_5"> <a href="mailto:info@ponsartsgrafiques.com">info@ponsartsgrafiques.com</a> </div>
							<p class="hero black bottommargin_0">93 804 47 91</p>
							<div class="toppadding_25"></div>
						</div>
						<div class="col-xs-12 col-sm-6 text-center text-sm-left">
							<div class="toppadding_30 visible-xs"></div>
							<div class="widget widget_mailchimp">
								<form class="signup form-inline" action="./" method="get">
									<div class="form-group-wrap">
										<div class="form-group"> <label for="mailchimp" class="sr-only"></label> <input name="email" type="email" id="mailchimp" class="mailchimp_email form-control" placeholder="Subscriu-te al butlletí"> <button type="submit" class="theme_button bg_button color2">Enviar</button>											</div>
									</div>
									<div class="response"></div>
								</form>
							</div>
							<div class="toppadding_10 visible-xs"></div>
						</div>
					</div>
				</div>
			</section>
			<section id="about" class="ls section_padding_top_150">
				<div class="container">
					<div class="row flex-wrap v-center">
						<div class="col-xs-12 col-md-7"> <img src="<?= base_url() ?>theme/images/about.png" alt="" class="cover-image"> </div>
						<div class="col-xs-12 col-md-5"> <span class="above_heading highlight">Benvinguts</span>
						<h2 class="section_header">Pons Art gràfiques és el líder en diferents formats d'impressió</h2>
						<p>Amb més de 50 anys d’experiència, estem especialitzats en la varietat de serveis d’alta qualitat.</p>
						<div class="toppadding_10 visible-md visible-lg"></div>
						<p> <a href="about.html" class="theme_button color1">Llegir més</a> </p>
					</div>
				</div>
			</div>
		</section>
		<section id="partners" class="ls">
			<div class="container">
				<div class="row flex-wrap v-center">
					<div class="col-xs-12 col-md-7 col-md-push-5"> <img src="<?= base_url() ?>theme/images/partners.png" alt="" class="cover-image"> </div>
					<div class="col-xs-12 col-md-5 col-md-pull-7"> <span class="above_heading highlight2">Clients</span>
					<h2 class="section_header">Els nostres clients son la nostra millor garantia</h2>
					<div class="toppadding_15"></div>
					<div class="isotope_container isotope row masonry-layout images-grid bordered columns_margin_bottom_20">
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner1.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner2.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner3.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner4.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner5.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner6.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner7.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner8.png" alt="">
						</a> </div>
						<div class="isotope-item col-xs-4"> <a href="#" class="partner-link">
							<img src="<?= base_url() ?>theme/images/partners/partner9.png" alt="">
						</a> </div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="services" class="ls section_padding_top_75 section_padding_bottom_120">
		<div class="container">
			<div class="row flex-wrap v-center">
				<div class="col-xs-12 col-md-7"> <img src="<?= base_url() ?>theme/images/services.jpg" alt="" class="cover-image"> </div>
				<div class="col-xs-12 col-md-5"> <span class="above_heading highlight3">Serveis</span>
				<h2 class="section_header">Estem especialitzats en una gran varietat de serveis</h2>
				<div class="toppadding_10"></div>
				<ul class="list1 no-bullets greylinks underlined-links color3">
					<li>
						<div class="media teaser media-icon-teaser">
							<div class="media-left media-middle">
								<div class="teaser_icon size_normal highlight3"> <i class="clr-service-copying"></i> </div>
							</div>
							<div class="media-body media-middle"> <a href="service-single.html">Impressió Ofsset</a> </div>
						</div>
					</li>
					<li>
						<div class="media teaser media-icon-teaser">
							<div class="media-left media-middle">
								<div class="teaser_icon size_normal highlight3"> <i class="clr-service-design"></i> </div>
							</div>
							<div class="media-body media-middle"> <a href="service-single.html">Serveis de Disseny</a> </div>
						</div>
					</li>
					<li>
						<div class="media teaser media-icon-teaser">
							<div class="media-left media-middle">
								<div class="teaser_icon size_normal highlight3"> <i class="clr-service-scaning"></i> </div>
							</div>
							<div class="media-body media-middle"> <a href="service-single.html">Impressió Digital</a> </div>
						</div>
					</li>
					<li>
						<div class="media teaser media-icon-teaser">
							<div class="media-left media-middle">
								<div class="teaser_icon size_normal highlight3"> <i class="clr-service-printing"></i> </div>
							</div>
							<div class="media-body media-middle"> <a href="service-single.html">Packaging</a> </div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="contact" class="cs parallax section_padding_top_150 section_padding_bottom_150">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-7 col-lg-6"> <span class="above_heading black">demanar pressupost</span>
			<h2 class="section_header">Normalment contestem en un termini d’una hora</h2>
			<form class="contact-form" method="post" action="./">
				<div class="row columns_margin_bottom_20">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group"> <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Nom"> </div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group"> <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email"> </div>
					</div>
					<div class="col-xs-12">
						<div class="form-group"> <input type="text" name="subject" id="subject" class="form-control" placeholder="Qué t'interesa?"> </div>
					</div>
					<div class="col-xs-12">
						<div class="form-group"> <textarea aria-required="true" rows="3" cols="45" name="message" id="message" class="form-control" placeholder="Missatge"></textarea> </div>
					</div>
					<div class="col-xs-12">
						<div class="contact-form-submit"> <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1 margin_0">Enviar pressupost</button> </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</section>
<section id="featured-video" class="cs main_color5 overlay_color parallax section_padding_top_150 section_padding_bottom_150">
<div class="container">
	<div class="row">
		<div class="col-sm-12 text-center">
			<a href="https://www.youtube.com/watch?v=2Kvl0vpV6lM" class="theme_button bg_button color3 round_button play_button margin_0" data-gal="prettyPhoto[gal-video]">
				<i class="fa fa-play" aria-hidden="true"></i>
			</a>
			<div class="toppadding_30 visible-lg"></div>
			<div class="toppadding_30"></div>
			<span class="above_heading highlight2">Video Presentació</span>
			<h2 class="section_header black">El nostre proces</h2>
		</div>
	</div>
</div>
</section>
<section id="products" class="ls section_padding_top_150 section_padding_bottom_145">
<div class="container">
<div class="row">
	<div class="col-xs-12 text-center"> <span class="above_heading highlight">Productes</span>
	<h2 class="section_header">Us ajudem a fer<br> el vostre projecte </h2>
	<div class="row flex-wrap loop-colors columns_padding_30 columns_margin_bottom_50">
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="teaser hover-teaser main_bg_color big-padding text-center">
				<div>
					<div class="teaser_icon highlight size_small"> <i class="clr-service-card"></i> </div>
					<h3 class="entry-title small bottommargin_0">Targetes de visita</h3>
					<p>Des de 9.99€</p>
					</div> <a href="shop.html" class="abs-link"></a> </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="teaser hover-teaser main_bg_color big-padding text-center">
						<div>
							<div class="teaser_icon highlight size_small"> <i class="clr-service-booklet"></i> </div>
							<h3 class="entry-title small bottommargin_0">Catàlegs</h3>
							<p>Des de 9.99€</p>
							</div> <a href="shop.html" class="abs-link"></a> </div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="teaser hover-teaser main_bg_color big-padding text-center">
								<div>
									<div class="teaser_icon highlight size_small"> <i class="clr-service-copy"></i> </div>
									<h3 class="entry-title small bottommargin_0">Impressió Digital</h3>
									<p>Des de 9.99€</p>
									</div> <a href="shop.html" class="abs-link"></a> </div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="teaser hover-teaser main_bg_color big-padding text-center">
										<div>
											<div class="teaser_icon highlight size_small"> <i class="clr-service-t-shirt"></i> </div>
											<h3 class="entry-title small bottommargin_0">Merchandaising</h3>
											<p>Des de 9.99€</p>
											</div> <a href="shop.html" class="abs-link"></a> </div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="teaser hover-teaser main_bg_color big-padding text-center">
												<div>
													<div class="teaser_icon highlight size_small"> <i class="clr-service-calendar"></i> </div>
													<h3 class="entry-title small bottommargin_0">Calendaris</h3>
													<p>Des de 9.99€</p>
													</div> <a href="shop.html" class="abs-link"></a> </div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-4">
													<div class="teaser hover-teaser main_bg_color big-padding text-center">
														<div>
															<div class="teaser_icon highlight size_small"> <i class="clr-service-banner"></i> </div>
															<h3 class="entry-title small bottommargin_0">Packaging</h3>
															<p>Des de 9.99€</p>
															</div> <a href="shop.html" class="abs-link"></a> </div>
														</div>
													</div>
													<div class="toppadding_20"></div>
													<div class="toppadding_20 visible-lg"></div> <a href="events-full.html" class="theme_button color1">Veure serveis</a> </div>
												</div>
											</div>
										</section>
										<section id="gallery" class="ls fluid_padding_0 columns_padding_0">
											<div class="container-fluid">
												<div class="row">
													<div class="col-xs-12">
														<div class="owl-carousel gallery-carousel" data-responsive-lg="3" data-responsive-md="3" data-responsive-sm="2" data-responsive-xs="2" data-responsive-xxs="1" data-loop="true" data-margin="0" data-center="true" data-nav="true">
															<div class="vertical-item gallery-item text-center">
																<div class="item-media-wrap">
																	<div class="item-media"> <img src="<?= base_url() ?>theme/images/gallery/01.jpg" alt=""> <a class="abs-link prettyPhoto" data-gal="prettyPhoto[gal]" title="" href="images/gallery/01.jpg">
																	</a> </div>
																</div>
																<div class="item-content">
																	<h3 class="entry-title small"><a href="gallery-single.html">BrightSky beauty salon brochures</a></h3> <span class="categories-links small-text highlightlinks"><a href="gallery-fullwidth-4-cols.html">brochures</a></span> </div>
																</div>
																<div class="vertical-item gallery-item text-center">
																	<div class="item-media-wrap">
																		<div class="item-media"> <img src="<?= base_url() ?>theme/images/gallery/02.jpg" alt=""> <a class="abs-link prettyPhoto" data-gal="prettyPhoto[gal]" title="" href="images/gallery/02.jpg">
																		</a> </div>
																	</div>
																	<div class="item-content">
																		<h3 class="entry-title small"><a href="gallery-single.html">Nulla a enim id nunc maximus ultricies</a></h3> <span class="categories-links small-text highlightlinks"><a href="gallery-fullwidth-4-cols.html">Bussiness Cards</a></span> </div>
																	</div>
																	<div class="vertical-item gallery-item text-center">
																		<div class="item-media-wrap">
																			<div class="item-media"> <img src="<?= base_url() ?>theme/images/gallery/03.jpg" alt=""> <a class="abs-link prettyPhoto" data-gal="prettyPhoto[gal]" title="" href="images/gallery/03.jpg">
																			</a> </div>
																		</div>
																		<div class="item-content">
																			<h3 class="entry-title small"><a href="gallery-single.html">Curabitur congue nulla metus</a></h3> <span class="categories-links small-text highlightlinks"><a href="gallery-fullwidth-4-cols.html">Flyers</a></span> </div>
																		</div>
																		<div class="vertical-item gallery-item text-center">
																			<div class="item-media-wrap">
																				<div class="item-media"> <img src="<?= base_url() ?>theme/images/gallery/04.jpg" alt=""> <a class="abs-link prettyPhoto" data-gal="prettyPhoto[gal]" title="" href="images/gallery/04.jpg">
																				</a> </div>
																			</div>
																			<div class="item-content">
																				<h3 class="entry-title small"><a href="gallery-single.html">Maecenas sed eleifend arcu</a></h3> <span class="categories-links small-text highlightlinks"><a href="gallery-fullwidth-4-cols.html">T-shirts</a></span> </div>
																			</div>
																			<div class="vertical-item gallery-item text-center">
																				<div class="item-media-wrap">
																					<div class="item-media"> <img src="<?= base_url() ?>theme/images/gallery/05.jpg" alt=""> <a class="abs-link prettyPhoto" data-gal="prettyPhoto[gal]" title="" href="images/gallery/05.jpg">
																					</a> </div>
																				</div>
																				<div class="item-content">
																					<h3 class="entry-title small"><a href="gallery-single.html">Vestibulum vel convallis sapien</a></h3> <span class="categories-links small-text highlightlinks"><a href="gallery-fullwidth-4-cols.html">Banners</a></span> </div>
																				</div>
																				<div class="vertical-item gallery-item text-center">
																					<div class="item-media-wrap">
																						<div class="item-media"> <img src="<?= base_url() ?>theme/images/gallery/06.jpg" alt=""> <a class="abs-link prettyPhoto" data-gal="prettyPhoto[gal]" title="" href="images/gallery/06.jpg">
																						</a> </div>
																					</div>
																					<div class="item-content">
																						<h3 class="entry-title small"><a href="gallery-single.html">Donec non dolor eget nisl</a></h3> <span class="categories-links small-text highlightlinks"><a href="gallery-fullwidth-4-cols.html">Finishing</a></span> </div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</section>
																<section id="testimonials" class="ls section_padding_top_150 section_padding_bottom_140">
																	<div class="container">
																		<div class="row">
																			<div class="col-xs-12 text-center">
																				<div class="owl-carousel testimonials-owl-carousel" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-dots="true" data-nav="false">
																					<blockquote>
																						<div class="avatar"> <img src="<?= base_url() ?>theme/images/faces/01.jpg" alt=""> </div>
																						<footer><cite>Mary J. McQuiston</cite><span class="small-text highlight">manager</span></footer>
																						<p>Colorway did an incredible job in creating our new marketing & sales pieces. Their prices are very competitive, and their quality is excellent. Thank you for the excellent service on the brochure reprints and the new display posters.</p>
																					</blockquote>
																					<blockquote>
																						<div class="avatar"> <img src="<?= base_url() ?>theme/images/faces/02.jpg" alt=""> </div>
																						<footer><cite>Ada B. Powell</cite><span class="small-text highlight">manager</span></footer>
																						<p>Thank you for the excellent service on the brochure reprints and the new display posters. I really appreciate your fine quality, speed and low price. Colorway has consistently demonstrated the ability to provide quality work.</p>
																					</blockquote>
																					<blockquote>
																						<div class="avatar"> <img src="<?= base_url() ?>theme/images/faces/03.jpg" alt=""> </div>
																						<footer><cite>Stephen I. Lebouef</cite><span class="small-text highlight">Businessman</span></footer>
																						<p>Colorway has consistently demonstrated the ability to provide quality work at a competitive price. We can always rely on them to have our printing completed on time and for acceptable price.</p>
																					</blockquote>
																				</div>
																			</div>
																		</div>
																	</div>
																</section>
																
																<?php $this->load->view('footer',array('es/footer'),FALSE,'paginas'); ?>
															</div>
															<!-- eof #box_wrapper -->
														</div>
<?php $this->load->view('es/scripts',array(),FALSE,'paginas'); ?>