<?php $act = empty($act)?1:$act; ?>
<!-- main nav start -->
<nav class="mainmenu_wrapper">
	<ul class="mainmenu nav sf-menu">
		<li class="<?= $act=='1'?'active':''; ?>">
			<a href="<?= base_url() ?>">
				<span>Inici</span>
			</a>
		</li>
		<li class="<?= $act=='2'?'active':'' ?>"> 
			<a href="<?= base_url() ?>nosaltres.html">
				<span>QUI SOM</span>
			</a>
		<li class="<?= $act=='3'?'active':'' ?>"> 
			<a href="<?= base_url() ?>serveis-i-productes.html">
				<span>SERVEIS I PRODUCTES</span>
			</a>
		<li class="<?= $act=='4'?'active':'' ?>"> 
			<a href="<?= base_url() ?>portfoli.html">
				<span>Portfoli</span>
			</a>
		<li class="<?= $act=='5'?'active':'' ?>">
			<a href="<?= base_url() ?>pujar-arxiu.html">
				<span>Pujar arxiu</span>
			</a>
		</li>
		<li class="<?= $act=='6'?'active':'' ?>">
			<a href="<?= base_url() ?>contacte.html">
				<span>Contacte</span>
			</a>
		</li>
		<!-- eof contacts -->
	</ul>
</nav>
<!-- eof main nav -->