<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
	<div id="box_wrapper">
		<?php $this->load->view('es/header',array('act'=>5),FALSE,'paginas'); ?>
			<section class="page_breadcrumbs ds background_cover section_padding_25">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h1>PUJAR ARXIUS</h1>
							<ol class="breadcrumb darklinks">
								<li> <a href="./">
							Home
						</a> </li>
								<!-- <li> <a href="#">Pages</a> </li> -->
								<li class="active"> <span>PUJA ELS TEU MATERIAL</span> </li>
							</ol>
						</div>
					</div>
				</div>
			</section>
			<section class="ls section_padding_top_150 section_padding_bottom_150">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="file-upload-wrapper cs main_color2 background_cover with_padding big-padding text-center">
								<h2 class="section_header">PUJADA DE ARXIUS</h2> <span class="under_heading grey">FINS A 100 Mb</span>
								<div class="toppadding_15"></div>
								<div class="toppadding_15 visible-md visible-lg"></div>
								<div class="toppadding_20 visible-lg"></div>
								<form class="contact-form file-upload-form input-text-center" method="post" action="./">
									<div class="row columns_margin_bottom_20">
										<div class="col-xs-12">
											<div class="inputfile"> <input type="file" size="30" name="file" id="file"> <label for="file" class="theme_button bg_button color1"><span>Carrega el teu fitxer</span></label> </div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group"> <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email"> </div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group"> <input type="tel" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Telèfon"> </div>
										</div>
										<div class="col-xs-12">
											<div class="form-group"> <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Nom"> </div>
										</div>
										<div class="col-xs-12">
											<div class="form-group"> <textarea rows="3" cols="45" name="message" id="message" class="form-control" placeholder="Missatge"></textarea> </div>
										</div>
										<div class="col-xs-12 topmargin_0">
											<div class="contact-form-submit"> <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1 margin_0">Enviar el teu fitxer</button> </div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>

			<?php $this->load->view('footer',array('es/footer'),FALSE,'paginas'); ?>
	</div>
	<!-- eof #box_wrapper -->
</div>
	<!-- eof #canvas -->
<?php $this->load->view('es/scripts',array(),FALSE,'paginas'); ?>