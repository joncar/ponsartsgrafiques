<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
	<div id="box_wrapper">
		<?php $this->load->view('es/header',array('act'=>6),FALSE,'paginas'); ?>		
			<section class="page_breadcrumbs ds background_cover section_padding_25">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h1>Contacte</h1>
							<ol class="breadcrumb darklinks">
								<li> <a href="./">
							Home
						</a> </li>
							<!-- 	<li> <a href="#">Pages</a> </li> -->
								<li class="active"> <span>Contacte</span> </li>
							</ol>
						</div>
					</div>
				</div>
			</section>
			<section id="contact-single" class="ls section_padding_top_150 section_padding_bottom_150 columns_padding_30 columns_margin_bottom_20">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-7 to_animate" data-animation="pullDown">
							<section id="map" class="ls ms" data-address=" Carrer Alemanya, 55 NAU 4." data-icon="<?= base_url('img/map-marker.png') ?>">
								<!-- marker description and marker icon goes here -->
								<div>
									<h3>On som?</h3>
									<p></p> 
									<img class="map_marker_icon" src="<?= base_url('img/map-marker.png') ?>" alt="">
								</div>
							</section>
						</div>
						<div class="col-xs-12 col-md-5 text-left text-lg-left to_animate" data-animation="pullDown"">
							<div class="inline-teasers-wrap">
								<div class="media teaser inline-block sm-block-media text-left">
									<div class="media-left">
										<div class="teaser_icon size_small main_bg_color2 round"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
									</div>
									<div class="media-body">
										<div class="toppadding_15"></div>
										<h4 class="topmargin_0">Adreça</h4>
										<div class="topmargin_minus_10"></div>
										<p class="small-text"> Carrer Alemanya, 55 NAU 4.<br> 08700 IGUALADA </p>
									</div>
								</div>
								<div class="toppadding_15 visible-md visible-lg"></div>
								<div class="media teaser inline-block sm-block-media text-left">
									<div class="media-left">
										<div class="teaser_icon size_small main_bg_color8 round"> <i class="fa fa-envelope" aria-hidden="true"></i> </div>
									</div>
									<div class="media-body">
										<div class="toppadding_15"></div>
										<h4 class="topmargin_0">Email</h4>
										<div class="topmargin_minus_10"></div>
										<p class="small-text"> info@ponsartsgrafiques.com<br>administracio@ponsartsgrafiques.com</p>
									</div>
								</div>
								<div class="toppadding_15 visible-md visible-lg"></div>
								<div class="media teaser inline-block sm-block-media text-left">
									<div class="media-left">
										<div class="teaser_icon size_small main_bg_color3 round"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
									</div>
									<div class="media-body">
										<div class="toppadding_15"></div>
										<h4 class="topmargin_0">Telèfon</h4>
										<div class="topmargin_minus_10"></div>
										<p class="small-text"> 93 804 47 91 </p>
									</div>
								</div>
								<div class="toppadding_15 visible-md visible-lg"></div>
								<div class="media teaser inline-block sm-block-media text-left">
									<div class="media-left">
										<div class="teaser_icon size_small main_bg_color round"> <i class="fa fa-clock-o" aria-hidden="true"></i> </div>
									</div>
									<div class="media-body">
										<div class="toppadding_15"></div>
										<h4 class="topmargin_0">Horaris</h4>
										<div class="topmargin_minus_10"></div>
										<p class="small-text"> Dilluns a Divendres<br> 9:00am - 8:00PM </p>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix toppadding_20"></div>
						<div class="col-xs-12 col-sm-12 to_animate" ">
							<div class="cs contact-form-wrapper background_cover with_padding big-padding">
								<form class="contact-form" method="post" action="./">
									<div class="row columns_margin_bottom_20 bottommargin_0">
										<div class="col-xs-12 col-sm-6">
											<div class="form-group"> <label for="name">Nom i cognoms</label> <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Nom i cognoms"> </div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group"> <label for="email">Email</label> <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email"> </div>
										</div>
										<div class="col-xs-12">
											<div class="form-group"> <label for="phone">Telèfon</label> <input type="tel" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Telèfon"> </div>
										</div>
										<div class="col-xs-12">
											<div class="form-group"> <label for="message">el teu missatge</label> <textarea aria-required="true" rows="3" cols="45" name="message" id="message" class="form-control" placeholder="el teu missatge"></textarea> </div>
										</div>
										<div class="col-xs-12 bottommargin_0">
											<div class="contact-form-submit"> <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1">Enviar Missatge</button> </div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php $this->load->view('footer',array('es/footer'),FALSE,'paginas'); ?>
	</div>
	<!-- eof #box_wrapper -->
</div>
	<!-- eof #canvas -->
<?php $this->load->view('es/scripts',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA"></script>