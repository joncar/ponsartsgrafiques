<!-- template sections -->
<div class="transparent_wrapper wrapper_v1">
	<section class="page_toplogo toplogo1 cs toggler_right columns_margin_0">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<a href="./" class="logo">
						<img src="<?= base_url() ?>theme/images/logo-light.png" alt="">
					</a>
					<span class="toggle_menu"><span></span></span>
					<span class="toggle_header visible-lg"><span></span></span>
				</div>
			</div>
		</div>
	</section>
	<div class="header_v1_wrapper">
		<header class="page_header header_white header_v1 toggler_xxs_right">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 display-flex v-center">
						<div class="header_left_logo"> <a href="./" class="logo">
							<img src="<?= base_url() ?>theme/images/logo.png" alt="">
						</a> </div>
						<div class="header_mainmenu text-center">
							<?= $this->load->view('es/menu',array(),FALSE,'paginas') ?>
						</div>
						<div class="header_right_buttons text-right">
							<!-- header toggler --><span class="toggle_header_close">tancar<span></span></span>
						</div>
					</div>
				</div>
			</div>
		</header>
		<section class="page_contacts cs main_color7 section_padding_top_110 section_padding_bottom_120 columns_margin_bottom_40 visible-lg">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="teaser text-center">
							<div class="teaser_icon size_huge highlight3"> <img src="<?= base_url() ?>theme/images/clr-phone-call-3.png" alt=""> </div>
							<p> <span class="small-text highlight3">TRUCA'NS ARA</span><br> <span class="big black">93 804 47 91</span> </p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="teaser text-center">
							<div class="teaser_icon size_huge highlight8"> <img src="<?= base_url() ?>theme/images/clr-mail-5.png" alt=""> </div>
							<p> <span class="small-text highlight8">CONTACTA'NS</span><br> <span class="big black darklinks"><a href="mailto:#">info@ponsartsgrafiques.com</a></span> </p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="teaser text-center">
							<div class="teaser_icon size_huge highlight9"> <img src="<?= base_url() ?>theme/images/clr-clock-5.png" alt=""> </div>
							<p> <span class="small-text highlight9">5 DIES A LA SETMANA</span><br> <span class="big black">9:00am - 8:00PM</span> </p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="page_social cs section_padding_top_110 section_padding_bottom_110 visible-lg">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<p class="page_social bottommargin_25"> <a href="#" class="social-icon light-bg-icon color-icon rounded-icon socicon-facebook"></a> <a href="#" class="social-icon light-bg-icon color-icon rounded-icon socicon-twitter"></a> <a href="#" class="social-icon light-bg-icon color-icon rounded-icon socicon-googleplus"></a>										</p>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>