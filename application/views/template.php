<!doctype html>
<html lang="en">
<head>
	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<script>window.wURL = window.URL; var URL = '<?= base_url() ?>'; var lang = '<?= $_SESSION['lang'] ?>';</script>	    
	
	<!-- Theme --->
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/animations.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/fonts.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/main.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/shop.css">
	<script src="<?= base_url() ?>theme/js/vendor/modernizr-2.6.2.min.js"></script>
	<!--[if lt IE 9]>
		<script src="js/vendor/html5shiv.min.js"></script>
		<script src="js/vendor/respond.min.js"></script>
		<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->
</head>

<body>	
	<div class="preloader">
		<div class="preloader_image"></div>
	</div>
	<?= $this->load->view($view);  ?>
</body>
</html>